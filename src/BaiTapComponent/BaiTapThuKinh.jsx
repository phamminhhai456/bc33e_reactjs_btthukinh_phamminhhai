import React, { Component } from "react";
import { data } from "./dataGlasses";

class BaiTapThuKinh extends Component {
  glasses = data;
  state = {
    glass: {
      name: "",
      desc: "",
      url: "",
    },
  };

  renderListGlasses = () => {
    return this.glasses.map((item, index) => {
      return (
        <img
          key={index}
          onClick={() => {
            this.handleChangeGlass(item);
          }}
          className="mx-2"
          style={{ width: "100px", cursor: "pointer" }}
          src={item.url}
          alt=""
        />
      );
    });
  };

  handleChangeGlass = (item) => {
    document.querySelector(".glass_info").style.display = "block";
    this.setState({
      glass: {
        name: `${item.name}`,
        desc: `${item.desc}`,
        url: `${item.url}`,
      },
    });
  };

  render() {
    let styleGlass = {
      width: "150px",
      top: "75px",
      right: "70px",
      opacity: "0.7",
    };

    let infoGlass = {
      width: "250px",
      top: "200px",
      left: "270px",
      paddingLeft: "15px",
      backgroundColor: "rgba(255,127,0,0.5)",
      textAlign: "left",
      height: "105px",
      display: "none",
    };

    return (
      <div
        style={{
          backgroundImage: "url(./glassesImage/background.jpg)",
          backgroundSize: "1400px",
          minHeight: "2000px",
        }}
      >
        <div
          style={{ backgroundColor: "rgba(0,0,0,0.5)", minHeight: "2000px" }}
        >
          <h3
            style={{ backgroundColor: "rgba(0,0,0,0.6)" }}
            className="text-center text-white py-5"
          >
            TRY GLASSES APP ONLINE
          </h3>

          <div className="container">
            <div className="row text-center">
              <div className="col-6 mt-5">
                <div className="position-relative">
                  <img
                    className="position-absolute"
                    style={{ width: "250px" }}
                    src="./glassesImage/model.jpg"
                    alt=""
                  />
                  <img
                    style={styleGlass}
                    className="position-absolute"
                    src={this.state.glass.url}
                    alt=""
                  />
                  <div
                    style={infoGlass}
                    className="position-relative glass_info"
                  >
                    <span
                      style={{ color: "#AB82FF" }}
                      className="font-weight-bold"
                    >
                      {this.state.glass.name}
                    </span>
                    <br />
                    <span className="text-white">{this.state.glass.desc}</span>
                  </div>
                </div>
              </div>
              <div className="col-6 mt-5">
                <img
                  style={{ width: "250px" }}
                  src="./glassesImage/model.jpg"
                  alt=""
                />
              </div>
            </div>
          </div>

          <div className="bg-light container mt-5 d-flex justify-content-center p-5">
            {this.renderListGlasses()}
          </div>
        </div>
      </div>
    );
  }
}

export default BaiTapThuKinh;
